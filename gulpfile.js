
'use strict';

//depencies
var gulp = require('gulp');
var sass = require('gulp-sass');
var minifyCSS = require('gulp-clean-css');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var changed = require('gulp-changed');
var browserSync = require('browser-sync');


var SCSS_SRC 	= 'spp/assets/scss/**/*.scss';
var SCSS_DEST 	= 'spp/assets/css';
var JS_SRC 		= 'spp/assets/js/*.js';
var JS_DEST 	= 'spp/assets/js';


//compiling
gulp.task('compile_scss', function(){
	gulp.src(SCSS_SRC)
		.pipe(sass().on('error', sass.logError))
		.pipe(minifyCSS())
		.pipe(rename({suffix: '.min'}))
		.pipe(changed(SCSS_DEST))
		.pipe(gulp.dest(SCSS_DEST))
});
gulp.task('compile_js', function(){
	gulp.src(JS_SRC)
		.pipe(uglify())
		.pipe(rename({suffix: '.min'}))
		.pipe(changed(JS_DEST))
		.pipe(gulp.dest(JS_DEST))
});
//detect changes
gulp.task('watch_scss', function(){
	gulp.watch(SCSS_SRC, ['compile_scss']);
});
gulp.task('watch_js', function(){
	gulp.watch(JS_SRC, ['compile_js']);
});


gulp.task('browser-sync', function() {
    browserSync.init({
        server:{
        	baseDir : 'spp/'
        }
    });
});

//run task
gulp.task('default', ['browser-sync','compile_scss','compile_js'], function(){
	gulp.watch('spp/**/*.html', browserSync.reload);
	gulp.watch('spp/assets/scss/*.scss', browserSync.reload);
	gulp.watch('spp/**/*.css', browserSync.reload);
	gulp.watch('spp/**/*.js', browserSync.reload);
	gulp.watch(SCSS_SRC, ['compile_scss']);
});
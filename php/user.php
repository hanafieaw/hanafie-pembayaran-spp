<?php require "funct.php"; include "header.php"; $fungi->cekSesi();?>

	<div class="container text-white kotakan bg-dark">
		<div style="width:600px; margin:auto; padding:30px">
			<h3>Profil</h3>
				<div class="form-group">
					Username <input type="text" name="username" class="form-control" value="<?php echo $fungi->cekUsername();?>" readonly>
				</div>
				
				<?php 
				
				$fungi->showModal("ganti","Ubah Password","
					<div class='form-group'>
						Password Lama <input type='password' name='password' size='10' class='form-control' placeholder='Masukan Password Lama' required>
					</div>
					<div class='form-group'>
						Password Baru <input type='password' name='password_baru' size='10' class='form-control' placeholder='Masukan Password Baru' required>
					</div>
				","Ubah");				
				?>
				
				<button class="btn btn-secondary" data-toggle="modal" data-target="#ganti">
					Ubah Password
				</button>
				<a href="dashboard.php" class="btn btn-secondary">Kembali	</a>
				<?php
					if(isset($_POST['Ubah'])){
					$pwn = md5($_POST['password_baru']);
					$pwl = md5($_POST['password']);
					
					$fungi->ubahPassword($pwl,$pwn);
				}
				?>
		</div>
	</div>
	
<?php include "footer.php";?>
<?php require "funct.php"; include "header.php"; $fungi->cekSesi();
	
	if(isset($_GET['nis'])){ 
		$nis = $_GET['nis'];
		$fungi->cekNIS($nis);
		
		$siswa = $fungi->open_portal->query("select * from siswa 
		where nis = '$nis'");
		$result = $siswa->fetch_assoc();		
	} 
	
?>

	<div class="container text-white kotakan bg-dark font-weight-bold ukuran-text">
		<h3>Transaksi</h3>
		<form method="post">
			<div class="form-group row">
				<label class="col-sm-2 col-form-label text-menu">NIS</label>
				<div class="col-sm-4">
					<input type="text" class="form-control" value="<?php echo $result['nis'];?>" readonly
					name="nis">
				</div>
				
				<label class="col-sm-1 col-form-label">Nama</label>
				<div class="col-sm-4">
					<input type="text" class="form-control" value="<?php echo $result['nama'];?>" readonly
					name="nama">
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">Tahun Ajaran</label>
				<div class="col-sm-4">
					<select name="ajaran" class="form-control">
						<option>2017/2018</option>
					</select>
				</div>
				
				<label class="col-sm-1 col-form-label">Bulan</label>
				<div class="col-sm-4">
					<select name="bulan" class="form-control">
						<?php	
							for ($i = 0; $i < 12; $i++) {
								$time = strtotime(sprintf('%d months', $i));   
								$label = date('F', $time);   
								echo "<option value='$label'>$label</option>";
							}
						?>
					</select>
				</div>
			</div>
			<div class="form-group row">	
				<label class="col-sm-2 col-form-label">Jumlah Yang Dibayar</label>
				<div class="col-sm-4">
					<div class="input-group mb-2">
						<div class="input-group-prepend">
							<div class="input-group-text">Rp</div>
						</div>
						<input type="number" class="form-control" name="jumlah_bayar">
					</div>
				</div>
					
				<label class="col-sm-1 col-form-label">Kelas</label>
				<div class="col-sm-4">
					<input type="text" class="form-control" value="<?php echo $result['kelas'];?>" readonly
					name="kelas">
				</div>
					
			</div>
			<div class="form-group row">
				<div class="col-sm-4">
					<button type="submit" class="btn btn-secondary" name="bayar_spp"
					data-toggle="modal" data-target="#bayar">Bayar</button>
					<a href="dashboard.php" class="btn btn-secondary">Kembali</a>
				</div>
			</div>
		</form>	
	</div>
		
	<div class="container text-white kotakan bg-dark">
	<?php	
		/* $fungi->showModal('bayar','Info Transaksi','vio','ya'); */
		
		if(isset($_POST['bayar_spp'])){			
			$nis_siswa = $_POST['nis']; $nama_siswa = $_POST['nama'];
			$ajaran = $_POST['ajaran']; $bulan= $_POST['bulan'];
			$jumlah = $_POST['jumlah_bayar'];
			
			$fungi->transaksi($nis_siswa,$bulan,$jumlah);
		}
	?>
		<h3>Histori Pembayaran</h3>
		
		<form method="post" class="form-inline">
			<div class="form-group">
				<input type="text" class="form-control" placeholder="Masukan Bulan . . ."> &nbsp
			</div>
			<div class="form-group">
				<button type="submit" class="btn btn-secondary">Cari</button>
			</div>
		</form>
			
		<div class="table-responsive" style="max-height:300px">
			<table class="table table-bordered table-dark">
				<thead>
					<th>No.</th> <th>Bulan</th> <th>Tgl Bayar</th> <th>Jumlah</th>
				</thead>
				<tbody>
					<?php $fungi->loadTransaksi($nis);?>
				</tbody>
			</table>
		</div>
	</div>

<?php include "footer.php";?>
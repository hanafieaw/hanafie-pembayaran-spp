<?php	
	class funct {
		
		function __construct(){	
			$this->open_portal = new mysqli("localhost","root","","data_spp");
			
			if($this->open_portal->connect_error){
				$this->showAlert("ERROR",$this->open_portal->connect_error,"alert-danger");		
			}
		}
		
		function showAlert($title,$isi,$tipe){
			echo "
				<div class='alert $tipe alert-dismissible' style='margin-top:5px;'>
					<button class='close' data-dismiss='alert'>&times;</button>
					<strong>".$title."</strong> - ".$isi."
				</div>
			";
		}
		
		function showModal($id,$title,$content,$button){
			echo "
				<form method='post'>
					<div class='modal text-dark' id='$id'>
					<div class='modal-dialog'>
						<div class='modal-content'>
							<div class='modal-header'>
								<h4 class='modal-title'>$title</h4>
								<button type='button' class='close' data-dismiss='modal'>&times;</button>
							</div>

							<div class='modal-body'>
								$content
							</div>
							
							<div class='modal-footer'>
								<button type='submit' name='$button' class='btn btn-primary'>$button</button>
								<button type='button' class='btn btn-danger' data-dismiss='modal'>Batal</button>
							</div>
						</div>
					</div>
					</div>
				</form>				
			";
		}
		
		function login(){
			if(isset($_POST['login_spp'])){
				$user = $_POST['username'];
				$pass = md5($_POST['password']);
				
				$result = $this->open_portal->query("select * from admin where password='$pass' and username = '$user'");
				
				if($result->num_rows == 1){
					$cek_super = $result->fetch_assoc();					
					$final = $cek_super['superadmin'];
					
					if($final == "0"){
						$_SESSION['admin_spp'] = $user;
						header("location:dashboard.php");
					}elseif($final == "1"){
						$_SESSION['superadmin_spp'] = $user;
						header("location:dashboard.php");
					}			
				}else{
					$this->showAlert("Gagal","Username/Password Salah","alert-danger");
				}
			}		
		}
		
		function logout(){
			session_destroy();
			header("location:index.php");
		}
		
		function reloading($file){
			echo("<script>location.href = '".$file."';</script>");
		}
		
		function hapusData($tabel,$field,$target){
			$hapus = $this->open_portal->query("delete from $tabel where $field='$target'");
			
			if(!$hapus){
				$this->showAlert("ERROR",$this->open_portal->error,"alert-danger");
			}
		}
		
		function cekUsername(){
			if(isset($_SESSION['admin_spp'])){
				return $_SESSION['admin_spp'];
			}elseif(isset($_SESSION['superadmin_spp'])){
				return $_SESSION['superadmin_spp'];
			}
		}
		
		function cekNIS($nis){
			$cek = $this->open_portal->query("select * from siswa
			where nis = '$nis'");
			$num_cek = $cek->num_rows;
			
			if($num_cek == 0){
				$this->showAlert("ERROR","NIS Tidak Ditemukan. 
				Silahkan Periksa Lagi. - <a href='transaksi.php'>Kembali</a>"
				,"alert-danger");
				exit;
			}
		}
		
		function ubahPassword($password,$password_baru){
			$user = $this->cekUsername();
			$cek_super = $this->open_portal->query("select * from admin where username = '$user'");
			$hasil_cek = $cek_super->fetch_assoc();					
			$id_admin = $hasil_cek['id_admin'];
			$password_lama = $hasil_cek['password'];
					
			if($password == $password_lama){
				$ubah = $this->open_portal->query("update admin set 
				password='$password_baru' where id_admin='$id_admin'");
			
				if(!$ubah){
					$this->showAlert("ERROR",$this->open_portal->error,"alert-danger"); 
				}else{
					$this->showAlert("Sukses","Password Telah Diubah","alert-primary");
					/* $this->reloading("user.php"); */
				}
			}else{
				$this->showAlert("ERROR","Password Lama Anda Salah !
				 Silakan Coba Lagi.","alert-danger");
			}
			
		}
		
		function cekSesi(){
			if(isset($_SESSION['admin_spp']) or isset($_SESSION['superadmin_spp'])){	
			}else{header("location:index.php");}
		}
		
		function tambahSiswa($nis,$nama,$kelas,$ajaran){
			$tambah = $this->open_portal->query("insert into siswa
			values('$nis','$nama','$kelas','$ajaran')");
			
			if(!$tambah){
				$this->showAlert("ERROR",$this->open_portal->error
				.' - <a href="siswa.php">Klik untuk refresh</a>',"alert-danger"); 
				exit;
			}else{$this->reloading("siswa.php");}
		}
		
		function tambahAdmin($username,$password){			
			$tambah = $this->open_portal->query("insert into admin values(null,
				'$username','$password','0')");
			
			if(!$tambah){
				$this->showAlert("ERROR",$this->open_portal->error,"alert-danger"); 
			}else{$this->reloading("admins.php");}	
		}
		
		function transaksi($nis,$bulan,$jumlah){
			$tgl_bayar = date("Y-m-d");
			$simpan = $this->open_portal->query("insert into pembayaran 
			values(null,'$nis','$bulan','$tgl_bayar','$jumlah')");
			
			if(!$simpan){
				$this->showAlert("ERROR",$this->open_portal->error
				.' - <a href="siswa.php">Klik untuk refresh</a>',
				"alert-danger");
			}else{$this->reloading("detil-transaksi.php?nis='$nis'");}
		}
	
		function loadAdmin(){
			$loads = $this->open_portal->query("select * from admin where id_admin <> 4");
			$nomor = 1; $psn = "'Apakah anda yakin ? Data akan dihapus PERMANEN'";
			while($row = $loads->fetch_assoc()){
				echo '
					<tr>
						<td>'.$nomor++.'</td>
						<td>'.$row['username'].'</td>
						<td>
							<a href="admins.php?del='.$row['id_admin'].'" class="btn btn-dark" 
							onclick="return confirm('.$psn.');">
								Hapus
							</a>
						<td>
					</tr>
				';
			}	
		}
		
		function loadSiswa($kelas){
			$loads = $this->open_portal->query("select * from siswa where kelas = '$kelas' order by nis asc");
			$nomor = 1; $psn = "'Apakah anda yakin ? Data akan dihapus PERMANEN'";
			
			if($loads->num_rows == 0){
				echo '
					<tr>
						<td colspan="6"><h4 class="text-white text-center">Data Kosong</h4></td>
					</tr>
				';
			}else{
				while($row = $loads->fetch_assoc()){
					echo '
						<tr>
							<td>'.$nomor++.'</td>
							<td>'.$row['nis'].'</td>
							<td>'.$row['nama'].'</td>
							<td>'.$row['kelas'].'</td>
							<td>'.$row['th_pelajaran'].'</td>
							<td>							
								<a href="siswa.php?del_siswa='.$row['nis'].'" class="btn btn-dark" 
								onclick="return confirm('.$psn.');">
									Hapus
								</a>
							</td>
						</tr>
					';
				}	
			}
			
		}
		
		function loadTransaksi($nis){
			$load = $this->open_portal->query("select * from pembayaran
			where nis = '$nis'");
			$nomor = 1;
			if($load->num_rows == 0){
				echo '
					<tr>
						<td colspan="6"><h4 class="text-white text-center">Data Kosong</h4></td>
					</tr>
				';
			}else{
				while($row = $load->fetch_assoc()){
					echo '
						<tr>
							<td>'.$nomor++.'</td>
							<td>'.$row['bulan'].'</td>
							<td>'.$row['tgl_bayar'].'</td>
							<td>'.$row['jumlah'].'</td>
						</tr>
					';
				}
			}
			
		}
	}
	
	
	/*---------------------------------------------------*/
	session_start();
	
	$fungi = new funct;
	
	if(isset($_POST['Ya'])){
		/* Logout */
		$fungi->logout();
	}elseif(isset($_GET['del'])){
		/* Hapus Admin */
		$id = $_GET['del'];
		$fungi->hapusData("admin","id_admin",$id);
		$fungi->reloading("admins.php");
	}elseif(isset($_GET['del_siswa'])){
		/* Hapus Admin */
		$id = $_GET['del_siswa'];
		$fungi->hapusData("siswa","nis",$id);
		$fungi->reloading("siswa.php");
	}
	
	/* ------------------------------------------------- */
?>
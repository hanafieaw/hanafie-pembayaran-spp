<?php require "funct.php"; include "header.php"; $fungi->cekSesi();
	
?>
	
	<div class="container text-white kotakan bg-dark">	
		<h2>Data Siswa</h2>		
		<form method="post">	
			<div class="form-group row" style="margin-left:10px;">
				<label class=" col-form-label"><b>Kelas</b></label>
				<div class="col-sm-1">
					<select name="tampil_kelas" id="tampil_kelas" class="form-control">
						<option>7</option>
						<option>8</option>
						<option>9</option>
					</select>
	
					<script type="text/javascript">
						document.getElementById('tampil_kelas').value = "<?php echo $_POST['tampil_kelas'];?>";
					</script>
				</div>
				<div class="col-sm-1">
					<button type="submit" class="btn btn-secondary" name="tampil">
					Tampilkan
					</button>
				</div>
			</div>			
		</form>
		
		<?php 
			$fungi->showModal("tambah","Tambah Siswa",
			"
				<form method='post'>
					<div class='form-group row'>
						<label class='col-sm-3 col-form-label'>Tahun Ajaran</label>
						<div class='col-sm-6'>
							<input type='text' name='ajaran' class='form-control' placeholder='2018/2019'>
						</div>
					</div>
					
					<div class='form-group row'>
						<label class='col-sm-3 col-form-label'>NIS</label>
						<div class='col-sm-6'>
							<input type='number' name='nis' class='form-control' placeholder='Nomor Induk Siswa'>
						</div>
					</div>
					
					<div class='form-group row'>
						<label class='col-sm-3 col-form-label'>Nama</label>
						<div class='col-sm-6'>
							<input type='text' name='nama' class='form-control' placeholder='Nama Siswa'>
						</div>
					</div>
					
					<div class='form-group row'>
						<label class='col-sm-3 col-form-label'>Kelas</label>
						<div class='col-sm-6'>
							<select name='kelas' class='form-control'>
								<option>7</option> <option>8</option> <option>9</option>
							</select>
						</div>
					</div>
				</form>
			","Tambah");
			
			if(isset($_POST['Tambah'])){
				$ajaran = $_POST['ajaran'];
				$nis = $_POST['nis'];
				$nama = $_POST['nama'];
				$kelas = $_POST['kelas'];
				
				$fungi->tambahSiswa($nis,$nama,$kelas,$ajaran);
				$fungi->loadSiswa($kelas);
			}
		?>
		
		<div class="table-responsive" style="max-height:400px;">
			<table class="table table-bordered table-dark">
				<thead>
					<th>NO</th> <th>NIS</th> <th>Nama</th> <th>Kelas</th> <th>Tahun Ajaran</th> 
					<th colspan="2">
						<button type="button" class="btn btn-secondary" 
						data-toggle="modal" data-target="#tambah">Tambah Siswa</button>
					</th>
				</thead>
				<tbody>
					<?php
						if(isset($_POST['tampil'])){
							$kelas = $_POST['tampil_kelas'];
							
							$fungi->loadSiswa($kelas);
						}else{$fungi->loadSiswa("7");}
					?>
				</tbody>
			</table>
		</div>
	</div>
<?php include "footer.php";
<!doctype html>
<html lang="en">
	<head>
		<title>Pembayaran SPP</title>				
		<meta name="viewport" content="width=device-width,initial-scale=1">
		<link rel="stylesheet" href="assets/css/bootstrap.min.css">
		<link rel="stylesheet" href="assets/css/style.css">
		<link rel="icon" href="assets/img/favicon.png">
	</head>
	
	<body>
		<div class="container-fluid bg-dark" style="padding:20px 0 10px 10px">
			<h1 class="text-white">Pembayaran SPP</h1>	
		</div>
	
		<?php 
			if(isset($_SESSION['admin_spp']) or isset($_SESSION['superadmin_spp'])){			
				echo '
					<nav class="navbar navbar-expand-sm bg-secondary justify-content-center">
						<ul class="navbar-nav font-weight-bold">
							<li class="navbar-item"><a href="dashboard.php" class="nav-link text-white">Beranda</a></li>
							<li class="navbar-item"><a href="user.php" class="nav-link text-white">Profil</a></li>
							<li class="navbar-item">
								<a href="#" data-toggle="modal" data-target="#logout" class="nav-link text-white">Logout</a>
							</li>				
						</ul>
					</nav>
				';
				$fungi->showModal("logout","Logout","
				<b><center>Apakah Anda Yakin</center></b>"
				,"Ya");
			}
		?>
		
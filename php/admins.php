<?php require "funct.php"; include "header.php"; 
	if(!isset($_SESSION['superadmin_spp'])){
		header("location:dashboard.php");
	}	
?>

	<div class="container text-white kotakan bg-dark">
		<h3>Daftar Admin</h3>
		<div class="table-responsive">
			<table class="table table-dark">
				<thead>
					<th>NO</th> <th>Username</th>
					<th>
						<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#tambah">Tambah Admin</button>
					</th>
				</thead>
				<tbody>
					<?php $fungi->loadAdmin();?>
				</tbody>
			</table>
			
			<?php		
			
				$fungi->showModal("tambah","Tambah Admin",
				'	<div class="form-group">
						Username <input type="text" class="form-control" name="username">
					</div>
					<div class="form-group">
						Password <input type="password" class="form-control" name="password">
					</div>
				'
				,"Selesai");
			
				if(isset($_POST['Selesai'])){
					$username = $_POST['username'];
					$password = md5($_POST['password']);
					
					$fungi->tambahAdmin($username,$password);
					$fungi->reloading("admins.php");	
				}
			?>
		</div>
	</div>

<?php include "footer.php";?>
<?php require "funct.php"; include "header.php"; $fungi->cekSesi();?>
	
	<div class="container text-white kotakan">		
		<div class="row justify-content-center">			
			<div class="col-sm-3 kotakan">
				<a href="transaksi.php">
					<div class="bg-secondary kotakmenu text-white">
						<img src="assets/img/transaksi.png" width="180px" height="180px">
						<b>TRANSAKSI</b>
					</div>
				</a>
			</div>
			
			<div class="col-sm-3 kotakan">	
				<a href="siswa.php">
					<div class="bg-secondary kotakmenu text-white">
						<img src="assets/img/siswa.png" width="180px" height="180px">
						<b><br>DATA SISWA</b>
					</div>
				</a>
			</div>
			
			<div class="col-sm-3 kotakan">
				<a href="rekap.php">
					<div class="bg-secondary kotakmenu text-white">
						<img src="assets/img/rekap.png" width="180px" height="180px">
						<b><br>REKAP DATA</b>
					</div>
				</a>
			</div>

			<?php
				if(isset($_SESSION['superadmin_spp'])){
					echo '
						<div class="col-sm-3 kotakan">
							<a href="admins.php">
								<div class="bg-secondary kotakmenu text-white">
									<img src="assets/img/users.png" width="180px" height="180px">
									<b><br>KELOLA ADMIN</b>
								</div>
							</a>
						</div>
					';
				}
			?>
		</div>			
	</div>
	
<?php include "footer.php";?>

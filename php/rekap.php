<?php require "funct.php"; include "header.php"; $fungi->cekSesi();?>

	<div class="container text-white kotakan bg-dark">
		<h3>Rekap Data</h3>
			<form method="post">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							Tanggal Awal : <input type="date" class="form-control">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							Tanggal Akhir : <input type="date" class="form-control">
						</div>
					</div>
				</div>
			<button type="submit" class="btn btn-secondary">Tampilkan</button>
		</form> 
	</div>
	
	<div class="container text-white kotakan bg-dark">
		
		<div class="row" style="margin-bottom:5px">
			<label class="col-sm-1 col-form-label">Kelas</label>
			<div class="col-sm-2">
				<select class="form-control">
					<option>7</option> <option>8</option> <option>9</option>
				</select>
			</div>
		</div>
		
		<div class="table-responsive" style="max-height:300px">
			<table class="table table-dark">
				<thead>
					<th>NO</th> <th>NIS</th> <th>NAMA</th> <th>KELAS</th> <th>BULAN</th> <th>JUMLAH</th>
					<th>STATUS</th>
				</thead>
			
				<tbody>
					
					<tr>
						<td colspan="5" class="text-center"><b>TOTAL</b></td>
						<td>Rp. 0</td>
					</tr>
				</tbody>			
			</table>	
		</div>
		
	</div>
	
<?php include "footer.php";?>
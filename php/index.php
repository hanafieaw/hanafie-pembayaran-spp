<?php require "funct.php"; include "header.php"; 
	if(isset($_SESSION['admin_spp'])){
		header("location:dashboard.php");
	}elseif(isset($_SESSION['superadmin_spp'])){
		header("location:superadmin.php");
	}
?>
		
		<div class="container text-white text-center">
			<div style="width:400px; margin:auto; padding:30px">
				<img src="assets/img/login.jpg" height="150px" width="150px" style="border-radius:80px; margin:8px;">
				<form method="post">
					<div class="form-group">
						<input type="text" class="form-control" name="username" placeholder="Username" required>
					</div>
				
					<div class="form-group">
						<input type="password" class="form-control" name="password" placeholder="Password" required>
					</div>
					<button type="submit" class="btn btn-secondary" name="login_spp"><b>Login</b></button>
					<?php $fungi->login();?>
				</form>
			</div>
		</div>
		
<?php include "footer.php";?>
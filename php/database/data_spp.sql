-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 08 Feb 2019 pada 10.39
-- Versi Server: 10.1.29-MariaDB
-- PHP Version: 7.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `data_spp`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(11) NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` text NOT NULL,
  `superadmin` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`id_admin`, `username`, `password`, `superadmin`) VALUES
(4, 'adminsuper', '21232f297a57a5a743894a0e4a801fc3', 1),
(5, 'admin', '71e23baee837c5d2952f93a29b113cab', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pembayaran`
--

CREATE TABLE `pembayaran` (
  `id` int(11) NOT NULL,
  `nis` int(17) NOT NULL,
  `bulan` varchar(45) NOT NULL,
  `tgl_bayar` date NOT NULL,
  `jumlah` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pembayaran`
--

INSERT INTO `pembayaran` (`id`, `nis`, `bulan`, `tgl_bayar`, `jumlah`) VALUES
(1, 1111, '2', '2019-02-08', 25000),
(3, 1111, '2', '2019-02-08', 50000),
(4, 1111, '3', '2019-02-08', 40000),
(5, 1111, '6', '2019-02-08', 333333),
(6, 1111, 'July', '2019-02-08', 60000),
(7, 1190, 'February', '2019-02-08', 25000),
(8, 1190, 'February', '2019-02-08', 222),
(9, 1190, 'May', '2019-02-08', 22222),
(10, 1190, 'February', '2019-02-08', 44444),
(11, 1190, 'February', '2019-02-08', 890),
(13, 1190, 'February', '2019-02-08', 0),
(14, 1111, 'February', '2019-02-08', 1234);

-- --------------------------------------------------------

--
-- Struktur dari tabel `siswa`
--

CREATE TABLE `siswa` (
  `nis` int(17) NOT NULL,
  `nama` varchar(35) NOT NULL,
  `kelas` varchar(5) NOT NULL,
  `th_pelajaran` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `siswa`
--

INSERT INTO `siswa` (`nis`, `nama`, `kelas`, `th_pelajaran`) VALUES
(1111, 'Angin Sore', '8', '2018/2019'),
(1190, 'Anpo', '9', '2018/2019'),
(9180, 'apep', '9', '2018/2019'),
(9901, 'Sani', '9', '2018/2019'),
(9910, 'siswa', '9', '2019/2020'),
(9919, 'dopw', '9', '2018/2019'),
(11890, 'asolpo', '7', '2018/2019'),
(12312, 'mopl', '9', '2018/2019'),
(12321, 'aaaaa', '9', '2019/2020');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `pembayaran`
--
ALTER TABLE `pembayaran`
  ADD PRIMARY KEY (`id`),
  ADD KEY `nis` (`nis`);

--
-- Indexes for table `siswa`
--
ALTER TABLE `siswa`
  ADD PRIMARY KEY (`nis`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `pembayaran`
--
ALTER TABLE `pembayaran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `pembayaran`
--
ALTER TABLE `pembayaran`
  ADD CONSTRAINT `pembayaran_ibfk_1` FOREIGN KEY (`nis`) REFERENCES `siswa` (`nis`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
